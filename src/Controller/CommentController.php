<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/", name="comment_index", methods={"GET"})
     */
    public function index(CommentRepository $commentRepository): Response
    {
        return $this->render('comment/index.html.twig', [
            'comments' => $commentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new-comment-ajax", name="comment_new_ajax", methods={"POST"})
     */
    public function new_ajax(Request $request)
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        
        if ($request->isXMLHttpRequest()) {   
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return new JsonResponse(array('data' => [
                    "comment_id" => $comment->getId(),
                    "comment_created_at" => $comment->getCreateAt(),
                    "comment_author" => $comment->getAuthor(),
                    "comment_content" => $comment->getContent()
                ])
            );
        }
    
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/{id}", name="comment_show", methods={"GET"})
     */
    public function show(Comment $comment): Response
    {
        return $this->render('comment/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    /**
     * @Route("/{id}", name="comment_edit_ajax", methods={"POST"})
     */
    public function edit_ajax(Request $request, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($request->isXMLHttpRequest()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('data' => [
                    "comment_id" => $comment->getId(),
                    "comment_created_at" => $comment->getCreateAt(),
                    "comment_author" => $comment->getAuthor(),
                    "comment_content" => $comment->getContent()
                ])
            );
        }

        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/{id}", name="comment_delete_ajax", methods={"DELETE"})
     */
    public function delete_ajax(Request $request, Comment $comment): Response
    {
        if ($request->isXMLHttpRequest()) {  
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
            return new Response('Comment successfully deleted!', 200);
        }

        return new Response('This is not ajax!', 400);
    }
}
